package framework.test;

import java.io.IOException;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import framework.pages.LoginPageSteps;
import framework.pages.Menu;
import framework.pages.PageFacto;
import framework.pages.TechnologyPage;

public class TechnologyPageTest extends CommonTests {
	
	
	//Test Case 7: To verify that a user can watch slides in a new window
	
	public TechnologyPageTest() throws IOException {
		super();
		// TODO Auto-generated constructor stub
	}

	@Test
	public void verifySlideInOtherWindow() throws InterruptedException
	{
		factory=PageFactory.initElements(driver,PageFacto.class);
		factory.steps.loginUrl();
		factory.steps.login("sanyam.arora92@gmail.com","asus@zenfone2");
		factory.m.clickontechnologyTab();
		Thread.sleep(5000);
		factory.tp.viewSymantecSlidesToOtherWindow();
	}

}
