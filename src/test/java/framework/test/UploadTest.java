package framework.test;

import java.awt.AWTException;
import java.io.IOException;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import framework.pages.LoginPageSteps;
import framework.pages.PageFacto;
import framework.pages.Upload;

public class UploadTest extends CommonTests
{

	public UploadTest() throws IOException {
		super();
		// TODO Auto-generated constructor stub
	}

	SoftAssert assertion=new SoftAssert();
	
	
	//Test Case 4: To upload a file inside your Slideshare account
	@Test
	public void doUploadTest() throws AWTException, InterruptedException
	{
		factory=PageFactory.initElements(driver,PageFacto.class);
		factory.steps.loginUrl();
		factory.steps.login("sanyam.arora92@gmail.com", "asus@zenfone2");
		factory.up.uploadingFile();
		factory.up.doUpload();
	}
	
	//Test Case 5: If a user wants to upload a file while he is not logged in, a pop-login window should come which enables him to log in and then update the file.
	//@Test
	public void loginPop() throws InterruptedException
	{
//		factory=PageFactory.initElements(driver,PageFacto.class);
//		factory.home.homeUrl();
//		factory.up.uploadingFile();
//		factory.lg.popUp("sanyam.arora92@gmail.com","asus@zenfone2");
//		factory.up.uploadingFile();
		
	}
}
