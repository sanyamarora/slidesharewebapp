package framework.test;


import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;



import com.perfectomobile.selenium.MobileDevice;
import com.perfectomobile.selenium.api.IMobileDriver;

import framework.pages.LoginPage;
import framework.pages.PageFacto;

//import pages.*;
public class LoginPageTest extends CommonTests
{




	public LoginPageTest() throws IOException {
		//super();
		// TODO Auto-generated constructor stub
	}



	//@Test
	//Test Case 1:To verify the login fields (field validation)
	public  void verifyText()
	{
		factory=PageFactory.initElements(driver,PageFacto.class);
		factory.lg.loginToSite();

		assertion.assertFalse(factory.lg.verifyUserName("abc"),"lg page username doesn't match");
		assertion.assertTrue(factory.lg.verifyPassword("cdf"),"lg page password doesn't match ");
		assertion.assertTrue(factory.lg.clickSubmit(), "lg page Submit Button doesn't clicked");
		assertion.assertAll();
	}



	//Test Case 2:To verify that username and password is invalid
	//@Test
	public void invalidlg() throws InterruptedException
	{
		factory=PageFactory.initElements(driver,PageFacto.class);
		factory.steps.loginUrl();
		factory.steps.login("abc","bcd");
		factory.steps.invalidwarningmessage();
		//assertion.assertAll();
	}

	//Test Case 3:To login into the site with valid user name and password
	//@Test
	public void loggedIn() throws InterruptedException
	{
		factory=PageFactory.initElements(driver,PageFacto.class);
		factory.steps.loginUrl();
		factory.steps.login("sanyam.arora92@gmail.com","asus@zenfone2");
	}



}
