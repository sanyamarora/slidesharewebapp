package framework.pages;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.omnitest.pages.WebPage;
import com.omnitest.util.GenericUtilities;
import com.omnitest.util.ReadSheet;

public class TechnologyPage extends SlideSharePage {
	
	String str[][];
	public TechnologyPage() throws Exception
	{
		addElementToPageElements("technology");
//		super();
//		str=ReadSheet.readSheet(System.getProperty("user.dir") + "//Slideshare.xls", "TechnologyPage");
	}
  
	public void viewSymantecSlidesToOtherWindow() throws InterruptedException
	{
		clickIfElementDisplayed("symantecLink");
		//switchtoFrame(driver.findElement(GenericUtilities.locatortype(str[2][2],str[2][1])));
		//waitOnXPath("frames");
		//clicking(str[3][2],str[3][1]);
		try
		{
			driver.getCurrentUrl();
			System.out.println(driver.getCurrentUrl());
		}
		catch(Exception e)
		{
			System.out.println("Failed to open new window");
		}
	}
	
}
