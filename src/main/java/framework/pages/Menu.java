package framework.pages;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.omnitest.util.GenericUtilities;
import com.omnitest.util.ReadSheet;


public class Menu extends SlideSharePage {

//	String str[][];
	public Menu() throws Exception
	{
		
		addElementToPageElements("menu");
	}
	
	public boolean clickonHomeTab()
	{
		try
		{
			if(isElementVisible("homeTab"))
			{
				clickIfElementDisplayed("homeTab");
			return true;
			}
		}
		catch(Exception e)
		{
			return false;
		}
		return false;
	}

	
	public boolean clickonleaderShip()
	{
		try
		{
			if(isElementVisible("leaderShipTab"))
			{
				clickIfElementDisplayed("leaderShipTab");
			return true;
			}
		}
		catch(Exception e)
		{
			return false;
		}
		return false;
	}
	
	
	public boolean clickontechnologyTab()
	{
		try
		{
			if(isElementVisible("technologyTab"))
			{
				clickIfElementDisplayed("technologyTab");
			return true;
			}
			
		}
		catch(Exception e)
		{
			return false;
		}
		return false;
	}
	
	
	public boolean clickoneducationTab()
	{
		try
		{
			if(isElementVisible("educationTab"))
			{
				clickIfElementDisplayed("educationTab");
			return true;
			}
		}
		catch(Exception e)
		{
			return false;
		}
		return false;
	}
	
	
	
	public boolean clickonmarketingTab()
	{
		try
		{
			if(isElementVisible("marketingTab"))
			{
				clickIfElementDisplayed("marketingTab");
			return true;
			}
		}
		catch(Exception e)
		{
			return false;
		}
		return false;
	}
}
