package framework.pages;

import java.io.IOException;

import org.openqa.selenium.By;




public class LoginPageSteps extends SlideSharePage {

	public static  LoginPageSteps loginSteps;
	String str[][];

	public	LoginPageSteps()  throws Exception
	{
		addElementToPageElements("loginSteps");
	}
	
	
	public void loginUrl()
	{
		System.out.println("we");
		driver.get("https://www.slideshare.net/login");
		System.out.println("The");
	}

	public void  login(String userNameValue,String passwordValue) throws InterruptedException
	{
		System.out.println("xpath");
		//addElementToPageElements("loginpageSteps");
		//driver.findElement(GenericUtilities.locatortype(str[1][2],str[1][1])).sendKeys("abc");;
		writeTextIntoTextBox("userName",userNameValue);
		writeTextIntoTextBox("password",passwordValue);
		clickIfElementDisplayed("submit");
		Thread.sleep(5000);
	}


	public boolean verifyLoginUrl()
	{

		try
		{
			String url=driver.getCurrentUrl();
			if(url.equalsIgnoreCase("http://www.slideshare.net/?ss"))
			{
				System.out.println("Sucessfully logged in");
			}
		}
		catch(Exception e)
		{
			return false;
		}
		return true;
	}
	public static LoginPageSteps getPage() throws Exception
	{
		if(loginSteps==null)
		{
			loginSteps=new LoginPageSteps();
		}
		return loginSteps;
	}
	public boolean invalidwarningmessage()
	{
		try
		{
			if(isElementVisible("warningMessage"))
			{
				System.out.println("Error message not coming");
				System.out.println(readTextFromElement("warningMessage"));
				return true;
			}
		}
		catch(Exception e)
		{
			System.out.println("Exception");
			return false;
		}
		return false;
	}
	

}
