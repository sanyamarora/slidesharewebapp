package framework.pages;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.xml.sax.SAXException;




public class LoginPage extends SlideSharePage {
	String str[][];


	public LoginPage() throws Exception
	{
		addElementToPageElements("loginpage");
		//	super();
		//	str=ReadSheet.readSheet(System.getProperty("user.dir") + "//Slideshare.xls", "LoginPage");
	}




	public void loginToSite()
	{
		driver.get("https://www.slideshare.net/login");
		//writeTextIntoTextBox("userName","abc");
	}



	public boolean verifyUserName(String user) 
	{ 
		try
		{
			if(isElementVisible("userName"))
			{
				//				pg.methods.enterText(userName,user);

				writeTextIntoTextBox("userName",user);
				return true;
			}
		}
		catch(Exception e)
		{
			System.out.println("Exception");
			return false;
		}
		return false;
	} 


	public boolean verifyPassword(String pass)
	{ 
		try
		{
			if(isElementVisible("password"))
			{
				writeTextIntoTextBox("password",pass);
				return true;
			}
		}
		catch(Exception e)
		{
			return false;
		}
		return false;
	} 


	public  boolean clickSubmit() 
	{
		try
		{
			if(isElementVisible("submit"))
			{
				clickIfElementDisplayed("submit");
				return true;
			}
		}

		catch(Exception e)
		{
			return false;
		}
		return false;
	} 

	public boolean invalidwarningmessage()
	{
		try
		{
			if(isElementVisible("warningMessage"))
			{
				System.out.println("Error message not coming");
				System.out.println(readTextFromElement("warningMessage"));
				return true;
			}
		}
		catch(Exception e)
		{
			System.out.println("Exception");
			return false;
		}
		return false;
	}
	
//		}
//		catch(Exception e)
//		{
//			System.out.println("Exception");
//			return false;
//		}
//		return false;
	









	//	public boolean invalidwarningmessage()
	//	{
	//		try
	//		{
	//			if(isElementVisible("warningMessage"))
	//					{
	//				          System.out.println(readTextFromElement("warningMessage"));
	//                          return true;
	//					}
	//		}
	//		catch(Exception e)
	//		{
	//			return false;
	//		}
	//		return false;
	//	}

	public void popUp(String user,String pass) throws InterruptedException
	{
		//switchtoFrame("frame");
		isElementVisible("frame");
		//		Thread.sleep(5000);
		clickIfElementDisplayed("loginLink");
		verifyUserName(user);
		verifyPassword(pass);
		clickSubmit(); 
		switchtoDefaultFrame();
	}




}
