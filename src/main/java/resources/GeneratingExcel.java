package resources;

import java.io.FileOutputStream;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;

public class GeneratingExcel {

	public static void main(String[] args) {

		try {
			String filename = "C:/Users/sanyam.arora/Desktop/CreateXcel.xls" ;
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet("FirstSheet"); 
			//style.setWrapText(true);
			//sheet.autoSizeColumn(6);
			//cell.setCellStyle(cs);
			HSSFRow rowhead = sheet.createRow((short)0);
			//rowhead.setRowStyle(style);
			rowhead.createCell(0).setCellValue("Page");
			rowhead.createCell(1).setCellValue("PageElement");
			rowhead.createCell(2).setCellValue("Type");
			rowhead.createCell(3).setCellValue("SubElement");
			rowhead.createCell(4).setCellValue("Type");
			rowhead.createCell(5).setCellValue("Actions");

			HSSFRow row = sheet.createRow((short)1);
			row.createCell(0).setCellValue("Home Page");
			row.createCell(1).setCellValue("View Slide");
			row.createCell(2).setCellValue("link");
			row.createCell(5).setCellValue("Write HomePage class.\nWrite viewslides() method.");


			HSSFRow row1 = sheet.createRow((short)2);
			row1.createCell(0).setCellValue("Login Page");
			row1.createCell(1).setCellValue("login");
			row1.createCell(2).setCellValue("Form");
			row1.createCell(3).setCellValue("username");
			row1.createCell(4).setCellValue("edit");
			row1.createCell(5).setCellValue("Write LoginPage class. \nWrite verifyuserName() method\nfor verifying username field.");
			
			HSSFRow row2 = sheet.createRow((short)3);
			row2.createCell(3).setCellValue("password");
			row2.createCell(4).setCellValue("edit");
			row2.createCell(5).setCellValue("Write verifyPassword() method for\nverifying password field.");
			
			HSSFRow row3 = sheet.createRow((short)4);
			row3.createCell(3).setCellValue("submit");
			row3.createCell(4).setCellValue("button");
			row3.createCell(5).setCellValue("Write clickSubmit() method for\nverifying submit button.");
			
			HSSFRow row4 = sheet.createRow((short)5);
			row4.createCell(5).setCellValue("Write login() method with\n3 steps for correct username\nand password fields and submit.");

			HSSFRow row5 = sheet.createRow((short)6);
			row5.createCell(1).setCellValue("loginerror");
			row5.createCell(2).setCellValue("text");
			row5.createCell(5).setCellValue("Write invalidwarningmessage() for\nverifying warning message.");


			FileOutputStream fileOut = new FileOutputStream(filename);
			workbook.write(fileOut);
			sheet.autoSizeColumn(5);
			//fileOut.close();
			System.out.println("Your excel file has been generated!");


		} catch ( Exception ex ) {
			System.out.println(ex);
		}
	}





}
